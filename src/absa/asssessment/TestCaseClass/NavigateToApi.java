/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absa.asssessment.TestCaseClass;

import absa.asssessment.DataDrivenClass;
import absa.asssessment.Instances;
import static absa.asssessment.Reports.FailPass_Results;
import absa.asssessment.XpathClass;
import java.io.FileNotFoundException;
import java.io.IOException;
import jxl.read.biff.BiffException;

/**
 *
 * @author Siphamandla
 */
public class NavigateToApi extends Instances {

    DataDrivenClass Data;

    public NavigateToApi(String Path) throws IOException, FileNotFoundException, BiffException {
        this.Data = new DataDrivenClass(Path);

    }

    public boolean ExecuteNavigate() throws IOException, BiffException, InterruptedException {
        if (!Navigate2WebTables()) {
            FailPass_Results("Could not Navigated to " + SeleniumActions.getText(XpathClass.AddUser()), "Failed");
            return false;
        }
        return true;
    }

    public boolean Navigate2WebTables() throws IOException, BiffException, InterruptedException {
        if (!SeleniumActions.Navigate2Browser("Chrome", Data.getData("Enviroment"))) {
            return false;
        }

        if (SeleniumActions.WaitforElementTimeOut(XpathClass.Search(), 1)) {
            FailPass_Results("Successfully Navigated to " + SeleniumActions.getText(XpathClass.AddUser()), "Passed");

        } else {
            return false;
        }

        return true;
    }

}
