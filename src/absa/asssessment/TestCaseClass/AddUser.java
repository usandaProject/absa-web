/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absa.asssessment.TestCaseClass;

import absa.asssessment.DataDrivenClass;
import absa.asssessment.Instances;
import static absa.asssessment.Reports.FailPass_Results;
import absa.asssessment.XpathClass;
import java.io.FileNotFoundException;
import java.io.IOException;
import jxl.read.biff.BiffException;

/**
 *
 * @author Siphamandla
 */
public class AddUser extends Instances {

    DataDrivenClass Data;

    public AddUser(String Path) throws IOException, FileNotFoundException, BiffException {

        this.Data = new DataDrivenClass(Path);

    }

    public boolean ExecuteAdd() throws IOException, BiffException, InterruptedException {
        if (!NoDuplicateUserName()) {
            FailPass_Results(Data.getData("Username") + " Already Exist", "Failed");
            return false;
        }
        if (!AddUser()) {
            FailPass_Results(Data.getData("Username") + " not added Succesfully.", "Failed");
            return false;
        }

        return true;
    }

    public boolean NoDuplicateUserName() throws IOException, BiffException, InterruptedException {

        if (!SeleniumActions.EnterByXpaths(XpathClass.Search(), Data.getData("Username"))) {
            return false;
        }
        if (!SeleniumActions.WaitforElementTimeOut(XpathClass.VerifyUsernameExistance(Data.getData("Username")), 2)) {
            FailPass_Results(Data.getData("Username") + " Does Not Exist ", "Pass");

        } else {
            return false;
        }

        return true;

    }

    public boolean AddUser() throws IOException, BiffException {
        if (!SeleniumActions.clickElement(XpathClass.AddUser())) {
            return false;
        }

        if (!SeleniumActions.EnterByXpaths(XpathClass.FirstName(), Data.getData("FirstName"))) {
            return false;
        }
        if (!SeleniumActions.EnterByXpaths(XpathClass.LastName(), Data.getData("LastName"))) {
            return false;
        }

        if (!SeleniumActions.EnterByXpaths(XpathClass.UserName(), Data.getData("Username"))) {
            return false;
        }

        if (!SeleniumActions.EnterByXpaths(XpathClass.Password(), Data.getData("Password"))) {
            return false;
        }

        if (Data.getData("Customer").equalsIgnoreCase("CompanyAAA")) {
            if (!SeleniumActions.clickElement(XpathClass.CompanyAAA())) {
                return false;
            }
        }else
        {
            if (!SeleniumActions.clickElement(XpathClass.CompanyBBB())) {
                return false;
            }
        }
        

        if (!SeleniumActions.clickElement(XpathClass.Roles(Data.getData("Role")))) {
            return false;
        }

        if (!SeleniumActions.EnterByXpaths(XpathClass.Email(), Data.getData("Email"))) {
            return false;
        }

        if (!SeleniumActions.EnterByXpaths(XpathClass.MobileNumber(), Data.getData("Cell"))) {
            return false;
        }

        if (!SeleniumActions.WaitbyXpath(XpathClass.SaveButton(), 3)) {
            return false;
        }

        if (!SeleniumActions.clickElement(XpathClass.SaveButton())) {
            return false;
        }

        if (!SeleniumActions.WaitbyXpath(XpathClass.AddUser(), 3)) {
            return false;
        }
        FailPass_Results(Data.getData("Username") + " Successfully Added", "Pass");
        return true;
    }

}
