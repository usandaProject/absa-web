/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absa.asssessment;

/**
 *
 * @author Siphamandla
 */
public class XpathClass {

    public static String Search() {
        return "//input[@placeholder='Search']";
    }

    public static String FirstName() {
        return "//input[contains(@name,'FirstName')]";
    }

    public static String LastName() {
        return "//input[contains(@name,'LastName')]";
    }

    public static String UserName() {
        return "//input[contains(@name,'UserName')]";
    }

    public static String Password() {
        return "//input[contains(@name,'Password')]";
    }

    public static String Roles(String text) {
        return "//select[@name='RoleId']//option[contains(text(),'" + text + "')]";
    }

    public static String CompanyAAA() {
        return "//label//input[@value=15]";
    }

    public static String CompanyBBB() {
        return "//label//input[@value=16]";
    }

    public static String Email() {
        return "//input[contains(@name,'Email')]";
    }

    public static String AddUser() {
        return "//button[contains(text(),'Add User')]";
    }

     public static String VerifyLandingPage() {
        return    "//span[text()='First Name']";
    }


    public static String MobileNumber() {
        return "//input[contains(@name,'Mobilephone')]";
    }

    public static String SaveButton() {
        return "//button[text()='Save' and not(@disabled)]";
    }

     public static String VerifyUsernameExistance(String text) {
         return "//i[@class='icon icon-remove']";
    }
    
    
    //td[text()='matt']
    
    //button[text()='Save']
}
