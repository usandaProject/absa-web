/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package absa.asssessment;

import static absa.asssessment.DataDrivenClass.TestCase;
import static absa.asssessment.Reports.actualFile;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Siphamandla
 */
public class SeleniumWebdriver {

    public static WebDriver Driver;
    public static int TestCount = 0;

    public SeleniumWebdriver() {
    }

    public void setWebDriverProperty() {
        System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");

    }
    private static void Set_Capabilities() {// setting Driver capabilities 
        Capabilities cap = ((RemoteWebDriver) Driver).getCapabilities();
        String BrowserType = cap.getBrowserName();


    }

    public static void DriverClose() {
        Driver.quit();

    }

    public boolean Navigate2Browser(String Browser, String Environment) {
        try {
            setWebDriverProperty();
   
            
            switch (Browser) {
                case "FireFox":
                    Driver = new FirefoxDriver();
                    Driver.get(Environment);
                    Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                    Driver.manage().window().maximize();
                case "Chrome":
                    Driver = new ChromeDriver();
                    Driver.get(Environment);
                    Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                    Driver.manage().window().maximize();
            }
        } catch (Exception e) {
            System.err.println("Faild to Launch Web Browser");
            return false;

        }
        return true;
    }

    public boolean WaitforElementTimeOut(String Xapth, int Seconds) {
        try {
            Driver.manage().timeouts().implicitlyWait(Seconds, TimeUnit.SECONDS);
            WebElement myDynamicElement = Driver.findElement(By.xpath(Xapth));
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public static void TakeScreenShot(String Screenshots) throws IOException {
        try {

            File scrFile = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(Screenshots + "\\" + TestCount + "_" + TestCase + ".jpg"));
            TestCount++;

        } catch (IOException e) {

            System.out.println("[Error] -  Failed to Take ScreenShot");
        }

    }

    public boolean EnterByXpaths(String xpath, String value) {
        try {

            Driver.findElement(By.xpath(xpath)).clear();
            Driver.findElement(By.xpath(xpath)).sendKeys(value);
            Thread.sleep(1000);
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    public boolean clickElement(String Xpath) {
        boolean Found = true;
        try {

            Driver.findElement(By.xpath(Xpath)).click();
            Thread.sleep(1000);
            return Found;
        } catch (InterruptedException E) {
            Found = false;
            return Found;
        }

    }

    public String getText(String Xpath) throws InterruptedException {
        boolean Found = true;
        try {
            Thread.sleep(1000);
            return Driver.findElement(By.xpath(Xpath)).getText();

        } catch (Exception e) {
        }
        return "";
    }

    public boolean WaitbyXpath(String Xapth, int time) {
        try {

            WebElement wait = (new WebDriverWait(Driver, time)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Xapth)));

        } catch (Exception e) {
            return false;
        }

        return true;
    }

    //span[text()='First Name']
}
